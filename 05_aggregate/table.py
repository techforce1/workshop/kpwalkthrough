"""
aggregeer consumed flows van topic ipflow2 en geef totaal weer

"""
import faust
import structprod

app = faust.App('table', topic_partitions=1, broker='kafka-1:19092')
topic = app.topic('ipflow2',value_type=structprod.IPFlow,partitions=1)
total_flows = app.Table('total_flows', default=int,partitions=1)

@app.agent(topic)
async def count_ipflows(ipflows):
    async for flow in ipflows:
        total_flows['total'] += 1
        print(f'{flow} is flow nr {total_flows["total"]}') 


if __name__=="__main__":
    app.main()
