"""
Simpele Faust based producer die:
- topic aanmaakt (in voorbeeld dynprod)
- dyanmisch bericht op dat topic produced 
"""
import faust
import random

# maak Faust app aan

app = faust.App('simpledynprod', broker='kafka-1:19092')
topic = app.topic('dynprod')

# registreer een async func (send_message):
# produceer een dynamische string (msg) elke (interval) seconden met een random ID

@app.timer(interval=1.0)
async def send_message(message):
    msg = "ID = "  + str(random.randint(0,10))
    await topic.send(value=msg)

if __name__ == '__main__':

    # Start de Faust async eventloop
    app.main()