"""
Simpele Faust based producer die:
- topic aanmaakt (in voorbeeld Tf1)
- bericht op dat topic produced 
"""
import faust

# maak Faust app aan
app = faust.App('simpleproducer', broker='kafka-1:19092')
topic = app.topic('Craftsmen', internal=True, partitions=1)

# registreer een async func (send_message):
# produceer een string (value) elke (interval) seconden
@app.timer(interval=1.0)
async def send_message(message):
    await topic.send(value="Event Processing met Python & Kafka")

if __name__ == '__main__':

    # Start de Faust async eventloop
    app.main()
